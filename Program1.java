

import java.io.*;

class Program1 {
	
	public static void main(String[] args)throws Exception{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Input: ");
		int x = Integer.parseInt(br.readLine());

		System.out.println("Output: ");

		for (int i = 10 ; i >= 1 ; i--){
			
			System.out.println(x + " X " + i + " = " + x*i);
		}
	}
}
